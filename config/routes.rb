Rails.application.routes.draw do

  resources :profiles, only: [:show]

  get 'welcome/index'



  ActiveAdmin.routes(self)
  
  resources :properties do
    resources :reviews, only: [:create]
  end

  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
  end

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  get 'get_comunes', to: 'properties#get_comunes', as: 'get_comunes'
  get 'user_properties', to: 'properties#user_properties', as: 'user_properties'
  # You can have the root of your site routed with "root"
  root 'welcome#index'
  
  # mailbox folder routes
  get "mailbox/inbox" => "mailbox#inbox", as: :mailbox_inbox
  get "mailbox/sent" => "mailbox#sent", as: :mailbox_sent
  get "mailbox/trash" => "mailbox#trash", as: :mailbox_trash

  post "get_map", to: "locations#get_map"
  
end