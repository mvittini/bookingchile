class City < ActiveRecord::Base
  acts_as_copy_target
  has_many :properties
  has_many :comunes

  def to_s
    self.name
  end
end
