class Comune < ActiveRecord::Base
  acts_as_copy_target
  belongs_to :city
  has_many :properties

  def to_s
    self.name
  end
end
