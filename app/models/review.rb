class Review < ActiveRecord::Base
  include ApplicationHelper
  belongs_to :property
  belongs_to :user

  validates :content, presence: true
end
