class Property < ActiveRecord::Base
  before_save :default_wifi, :default_parking
  after_create :fill_capacity

  belongs_to :user
  belongs_to :comune
  belongs_to :city
  has_many :reviews

  validates :room, :mt2 ,:double_bed ,:single_bed ,:cabin ,:bathroom ,:description , :city_id, :address, :comune_id, presence: true



  geocoded_by :full_address
  after_validation :geocode
  mount_uploader :images, ImagesUploader

  def default_wifi
    wifi ||= false
    return
  end

  def default_parking
    parking ||= false
    return
  end

  def full_address
    [address, comune.name , city.name, 'chile' ].compact.join(', ')
  end

  def fill_capacity
    self.capacity = (double_bed*2) + (cabin*2)+ single_bed
    self.save  
  end

end