class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_save :default_role, :default_premium, :rut_normalize
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  acts_as_messageable

  has_many :property, dependent: :destroy
  has_many :reviews

  mount_uploader :avatar, AvatarUploader
  
  validates :name, :lastname, :mlastname, :birthdate, presence: true
  validates :username, presence: true, uniqueness: { case_sensitive: false }
  #validates :rut ,presence: true, uniqueness: { case_sensitive: false }, rut: true

  enum role: [:admin, :owner, :client, :guest]


  def mailboxer_name
    self.name
  end

  def mailboxer_email(object)
    self.email
  end
  
  def default_role
  	self.role ||= 2
  end
  
  def default_premium
    self.premium ||= false
    return
  end

  def to_s
    "#{self.name} #{self.lastname}"
  end

  def rut_normalize

  end
end
