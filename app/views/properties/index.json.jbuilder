json.array!(@properties) do |property|
  json.extract! property, :id, :room, :mt2, :double_bed, :single_bed, :cabin, :bathroom, :description, :images, :address, :latitude, :longitude, :user_id, :comune_id
  json.url property_url(property, format: :json)
end
