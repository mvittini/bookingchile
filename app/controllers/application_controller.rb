class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  helper_method :mailbox, :conversation
  
  private

    def mailbox
      @mailbox ||= current_user.mailbox
    end
    
    def conversation
      @conversation ||= mailbox.conversations.find(params[:id])
    end

	  def configure_permitted_parameters
	    devise_parameter_sanitizer.for(:sign_up) << [:avatar, :rut, :name , :lastname , :mlastname, :birthdate, :premium, :role]
	    devise_parameter_sanitizer.for(:account_update) << [:avatar, :rut, :name , :lastname , :mlastname, :birthdate, :premium, :role]
	  end
end
