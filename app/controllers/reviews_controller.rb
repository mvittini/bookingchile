class ReviewsController < ApplicationController
  load_and_authorize_resource

  def create
    property = Property.find(params[:property_id])

    @review = property.reviews.build(review_params)
    @review.user = current_user if user_signed_in?
    @review.save

    redirect_to property
  end
  
  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Property was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:content)
    end
end
