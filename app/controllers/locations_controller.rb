class LocationsController < ApplicationController
	def get_map

    address = params[:full_address]
    @properties = Property.near(address, 50)

    @markers = Gmaps4rails.build_markers(@properties) do |property, marker|
      marker.lat property.latitude
      marker.lng property.longitude
      marker.title property.title
      marker.picture({
        url: "#{view_context.image_path("cabin-2.png") }",
        width:  32,
        height: 37
      })
      # marker.room property.room
      # marker.mt2 property.mt2
      # marker.double_bed property.double_bed
      # marker.single_bed property.single_bed
      # marker.cabin property.cabin
      # marker.bathroom property.bathroom
      # marker.description property.description
      # marker.images property.images
      # marker.address property.address
      # marker.user_id property.user_id
      # marker.wifi property.wifi
      # marker.parking property.parking
      # marker.capacity property.capacity
      # marker.type property.type
    end

    address_coord = Geocoder.coordinates(address)

    @marker = {
      lat: address_coord[0],
      lng: address_coord[1],
      picture: {
        url: "#{view_context.image_path("direction_down.png") }",
        width:  32,
        height: 37
      }
    }


    respond_to do |format|
      format.js
    end
  end

end
