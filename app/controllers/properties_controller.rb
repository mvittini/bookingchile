class PropertiesController < ApplicationController
  load_and_authorize_resource except: :get_comunes
  before_action :set_property, only: [:show, :edit, :update, :destroy]
  before_action :select_location
  # GET /properties
  # GET /properties.json
  def index
    @properties = Property.all
  end

  # GET /properties/1
  # GET /properties/1.json
  def show
    @reviews = @property.reviews.reverse
  end

  # GET /properties/new
  def new
    @property = Property.new
  end

  # GET /properties/1/edit
  def edit
  end

  def get_comunes
    @comunes = Comune.where("city_id = ?", params[:city_id])
    respond_to do |format|
     format.js
    end
  end



  

    
  
  # POST /properties
  # POST /properties.json
  def create
    @property = Property.new(property_params)
    @property.user = current_user if user_signed_in?
    

    respond_to do |format|
      if @property.save
        current_user.owner!
        format.html { redirect_to @property, notice: 'Property was successfully created.' }
        format.json { render :show, status: :created, location: @property }
      else
        format.html { render :new }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/1
  # PATCH/PUT /properties/1.json
  def update
    respond_to do |format|
      if @property.update(property_params)
        format.html { redirect_to @property, notice: 'Property was successfully updated.' }
        format.json { render :show, status: :ok, location: @property }
      else
        format.html { render :edit }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/1
  # DELETE /properties/1.json
  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Property was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_property
      @property = Property.find(params[:id])
    end
   

    def select_location
      @cities = City.all
      @comunes = Comune.where("city_id = ?", City.first.id)
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def property_params
      params.require(:property).permit(:room, :mt2, :double_bed, :single_bed, :cabin, :bathroom, :description, :images, :address, :latitude, :longitude, :user_id, :city_id, :comune_id, :wifi, :parking, :capacity, :type, :full_street_address, :images_cache)
    end
end
