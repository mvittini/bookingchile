$(document).on("page:change", function() {
	get_map('chile')
});

$(document).on("page:change", function() {

	$('.city_map').change(function(event) {
		set_address();
	});

	$('.comune_map').change(function() {
		set_address();
	});

	$('.address_map').keypress(function(event) {
		key = event.which;
		if(key == 13) {
			set_address();
			return false;
		}
	});
});

function set_address () {
	var address = $('.address_map').val();
	var city = $('.city_map').find(':selected').text();
	var comune = $('.comune_map').find(':selected').text();
	var full_address = address + ', ' + comune + ', ' + city + ', chile';
	console.log(full_address);
	get_map(full_address)
}

function get_map (full_address) {
	$.ajax({
		url: '/get_map',
		type: 'POST',
		dataType: 'script',
		data: {full_address: full_address}
	});
}
