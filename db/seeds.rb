    # This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Review.destroy_all
Property.destroy_all
User.destroy_all
Comune.destroy_all
City.destroy_all

City.copy_from"db/chile/cities.csv"
Comune.copy_from"db/chile/comunes.csv", map: {"name" => "name","city_id" => "city_id","id" => "id"}



admin = User.create(
  name: 'Marco',
  remote_avatar_url: 'http://lorempixel.com/1200/800',
  username: 'Mvittini',
  lastname: 'Vittini',
  mlastname: 'Quilodran',
  email: 'mvittini@live.cl',
  password: 'password',
  birthdate:Faker::Date.between(80.years.ago, 22.years.ago),
  role: 0
)

users = []

(1..20).each do |i|
  users << User.create(
    name: Faker::Name.first_name,
    username: Faker::Internet.user_name,
    lastname: Faker::Name.last_name,
    mlastname: Faker::Name.last_name,
    birthdate:Faker::Date.between(80.years.ago, 22.years.ago),
    email: "cliente_#{i}@live.cl",
    password: 'password',
    role: 1
  )
end

properties = []

50.times do |r|
	city = City.all.sample.id
	properties << Property.create(
		user_id: User.all.sample.id,
		room: Faker::Number.between(1, 4),
		mt2: Faker::Number.between(30, 150),
		double_bed: Faker::Number.between(0, 3),
		single_bed: Faker::Number.between(1, 6),
		cabin: Faker::Number.between(0, 3),
		bathroom: Faker::Number.between(1, 3),
		description: Faker::Lorem.paragraph(2, false, 4),
		remote_images_url: 'http://lorempixel.com/1200/800',
		address: Faker::Address.street_address,
		city_id: city,
		comune_id: Comune.where(city_id: city).sample.id,
		wifi: rand(2),
		parking: rand(2)

	)
end


reviews = []

150.times do |r|
  reviews << Review.create(
    user: users.sample,
    property: properties.sample,
    content: Faker::Hacker.say_something_smart
  )
end

