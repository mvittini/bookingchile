class AddDetailsToProperty < ActiveRecord::Migration
  def change
    add_column :properties, :wifi, :boolean
    add_column :properties, :parking, :boolean
    add_column :properties, :capacity, :integer
    add_column :properties, :type, :string
  end
end
