class AddCityToProperty < ActiveRecord::Migration
  def change
    add_reference :properties, :city, index: true, foreign_key: true
  end
end
