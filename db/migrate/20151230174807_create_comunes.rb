class CreateComunes < ActiveRecord::Migration
  def change
    create_table :comunes do |t|
      t.string :name
      t.references :city, index: true, foreign_key: true

    end
  end
end
