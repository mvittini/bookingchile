class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.integer :room
      t.integer :mt2
      t.integer :double_bed
      t.integer :single_bed
      t.integer :cabin
      t.integer :bathroom
      t.text :description
      t.string :images
      t.string :address
      t.float :latitude
      t.float :longitude
      t.references :user, index: true, foreign_key: true
      t.references :comune, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
