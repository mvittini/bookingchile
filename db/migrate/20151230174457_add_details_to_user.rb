class AddDetailsToUser < ActiveRecord::Migration
  def change
  	add_column :users, :rut, :string
    add_column :users, :name, :string
    add_column :users, :lastname, :string
    add_column :users, :mlastname, :string
    add_column :users, :birthdate, :date
    add_column :users, :premium, :boolean
    add_column :users, :role, :integer
    add_column :users, :username, :string
  end
end
